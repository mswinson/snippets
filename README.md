##README

my code snippets

```
  unknown        - snippets for 'unknown' file types (no extension)

  gitignore      - gitignore snippets

  html           - HTML snippets
  javascript     - Javascript snippets
  markdown       - Markdown snippets
  ruby           - Ruby snippets
  python         - Python snippets
  sh             - Shell snippets
```
