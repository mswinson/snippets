set rtp+=~/.snipMate/

let g:snipMate = {}
let g:snipMate.scope_aliases = {}
let g:snipMate.scope_aliases['ruby'] = 'ruby,yard,rubcop,gemfile'
let g:snipMate.scope_aliases['javascript'] = 'jasmine,backbone,grunt,jsdoc'
let g:snipMate.scope_aliases['html'] = 'html,bootstrap,loremipsum'
let g:snipMate.scope_aliases['markdown'] = 'readme'
let g:snipMate.scope_aliases['cpp'] = 'cpp'
let g:snipMate.scope_aliases['gitignore'] = 'node,rubyignore,dbignore'


au BufNewFile *.sh :call feedkeys("ibashfile\<tab>")
au BufNewFile *.html :call feedkeys("ifile\<tab>")
au BufNewFile *_spec.rb :call feedkeys("itestcase\<tab>")
au BufNewFile *_spec.js :call feedkeys("itestcase\<tab>")
au BufNewFile Gemfile :call feedkeys("igemfile\<tab>")
